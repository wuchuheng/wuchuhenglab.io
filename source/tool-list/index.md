title: 工具
date: 2020-03-30 07:33:52
---
## 1 开发类

#### 1.1.1 navicat
{% note primary %}
国产数据库软件
[【mac】navicat](http://qiniu.wuchuheng.com/Navicat_Premium_15.0.12_macwk.com.dmg)
[【windows】navicat](http://qiniu.wuchuheng.com/navicat%E4%B8%AD%E6%96%87%E7%89%88%E7%A0%B4%E8%A7%A3.zip)
{% endnote %}

#### 1.1.2 粘贴板

{% note success %}
clipx是一简单实用粘贴板工具。没有花销的功能，单纯做好一款粘贴板工具
[官方网址](https://bluemars.org/clipx/)
[【windowns】clipx](http://qiniu.wuchuheng.com/clipx.exe)
{% endnote %}

#### 1.1.3 ssh终端工具putty
{% note note %}
putty是一款简单实用终端连接工具，同上一样，就做好这个功能，没乱七杂八的东西，好用。
[官方网址](https://www.putty.org/)
[【windowns】putty](http://qiniu.wuchuheng.com/putty.exe)
{% endnote %}

#### 1.1.3 ftp上传工具winScp
{% note danger %}
    winScp是一款简单实用终端连接工具，同上一样，真好用。我就想安安静静地上传个文件，不想整些乱七八糟的配置。同其它的工具对比下，winScp亲切多了。
    但也是有缺点的，算是偏爱吧，不接受反驳和意见。
    [官方网址](https://winscp.net/eng/index.php)
    [【windowns】winScp](http://qiniu.wuchuheng.com/WinSCP-5.13.3-Setup.exe)
{% endnote %}

#### 1.1.4 postman接口测试工具 
{% note  primary %}
    经典的接口测试工具
    [官方网址](https://www.postman.com/)
    [【windowns】winScp](http://qiniu.wuchuheng.com/PostmanCanary-win64-7.22.0-canary05-Setup.exe)
{% endnote %}

#### 1.1.5 bestTrace IP路由地图可视化工具 
{% note default %}
    ip路由地图测试工具，用于测试当前ip数据包到目标ip的传送路线。
    [官方网址](https://tools.ipip.net/convert.php)
    [【windowns】bestTrace](http://qiniu.wuchuheng.com/MTracer.zip)
{% endnote %}

#### 1.1.6 镜像写入工具
{% note info %}
    Win32DiskImager-0.9.5-binary 镜像写入工具，用于树梅派系统烧录内存卡系统，也可用于u盘制作
    [下载网址](https://sourceforge.net/projects/win32diskimager/)
    [【windowns】 Win32DiskImager](http://qiniu.wuchuheng.com/Win32DiskImager-0.9.5-binary.zip)
{% endnote %}
{% note info %}
     rufus-3.3.exe镜像写入工具,用于u盘制作,u盘制作上相对于win32DiskImager更专业些
    [官方网址](https://rufus.ie/)
    [【windowns】refus](http://qiniu.wuchuheng.com/rufus-3.3.exe)
{% endnote %}


#### 1.1.7 端口扫描
{% note success  %}
    `nmap`端口扫描工具，用于从使用者的角度来看对方`ip`的开放情况，双刃利器。
    [官方网址](https://nmap.org/)
    [【windowns】nmap](http://qiniu.wuchuheng.com/nmap-7.80-win32.zip)
{% endnote %}

#### 1.1.8 抓包工具

{% note warning  %}
    `Wireshark`抓包一哥，没有什么数据包搞不定的，`udp,tcp/ip`协议簇下的协议都搞,不仅本地搞,还能远程在服务器上搞。
    [官方网址](https://www.solarwinds.com/)
    [【windowns】wireshark](http://qiniu.wuchuheng.com/Wireshark-win64-3.0.6.exe)
{% endnote %}

{% note warning  %}
    `http`抓包工具`fiddler`
    [【windowns】fiddler](http://qiniu.wuchuheng.com/fiddler5.zip)
{% endnote %}

#### 1.1.9 磁盘重置化工具

{% note info %}
    硬盘重置化工具HDDLLFsetup，如果硬盘格式化不了，这个或许还能抢救下！
    [【windowns】HDDLLFsetup](http://qiniu.wuchuheng.com/HDDLLFsetup.4.40.exe)
{% endnote %}

