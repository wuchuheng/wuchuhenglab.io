title: 书单
date: 2019-11-21 23:26:56
---
#### 技术向

##### [《angular官方文档》](https://angular.io/start) 进度1%
##### [《笨方法学习vim》](http://learnvimscriptthehardway.stevelosh.com/) 进度100% （注：没读透）
##### 《vim实用技巧（第2版）》进度100%  (注：没读透)
##### [《vim 入门到精通》](https://github.com/mhinz/vim-galore#readme) 进度100% （注：没读透）
##### 《计算机科学导论》进度7.25%
##### 《C程序设计语言》 进度2.7%
##### 《Docker容器与容器云(第2版)》 进度1%
##### 《代码简洁之道》 进度1%
##### [《ngx-admin》](https://akveo.github.io/ngx-admin/docs/getting-started/what-is-ngxadmin#what-is-ngxadmin)  进度0%
##### [《nebular》](https://akveo.github.io/nebular/?utm_source=ngx_admin_landing&utm_medium=docs_getting_started) 进度 0%

##### [《图说设计模式》](https://design-patterns.readthedocs.io/) 进度 1% 优先等级: 最高


#### 文学向

##### 《三体》 进度90%


#### 哲学向

##### 《道德经》 进度5%

#### 语言类 

##### 《英语新思维初级》进度100% （注:没读透）
##### 《英语词根词缀与单词的说文解字》进度 29.7%
##### 《小韦绿》 进度0%
##### 《单挑》 进度0%
