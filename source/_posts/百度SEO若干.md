---
layout: layout
title: 「SEO」百度SEO若干
date: 2018-06-05 08:43:00
category: SEO
tags: SEO
doc: true
---
### 禁用百度转码声明
``` html
<head>
    <meta http-equiv="Cache-Control" content="no-transform" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
</head>
```
<!--more-->
